<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;


//image form
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Image;
use App\Form\AddImageType;
use Doctrine\ORM\EntityManagerInterface;
use DateTime;

//albums
use App\Repository\AlbumRepository;
use App\Repository\ImageRepository;
class DefaultController extends AbstractController
{

    public function __construct(private AlbumRepository $albumRepository, private ImageRepository $imageRepository){

    }
    #[Route('/', name: 'app_default')]
    public function index(): Response
    {
        $albums = $this->albumRepository->findAll();

        return $this->render('default/index.html.twig',['albums'=>$albums]);
    }

    #[Route('/add-image', name: 'app_add_image')]
    public function addImage(Request $request,EntityManagerInterface $entityManager): Response
    {
        $image = new Image();
        $form = $this->createForm(AddImageType::class, $image);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            
            $image->setDateCreation(new DateTime());
            $entityManager->persist($image);
            $entityManager->flush();
            
            return $this->redirectToRoute('app_default');
           /* return $this->redirectToRoute('image_list');*/
        }

        return $this->render('default/addimage.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/albums/{slug}', name: 'app_album')]
    public function album($slug): Response
    {
        $album = $this->albumRepository->findOneBy(['slug' => $slug]);
        $error = '';
        if($album && $album->isIsPublic()){
            $images = $album->getImages();
        }else{
            $error = 'aucun album ou album est privée';
            $images = [];
        }
        


        return $this->render('default/album.html.twig', ['images'=> $images,'error'=>$error]);
    }
}
