<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

use App\Entity\Album;
use App\Entity\Image;
use App\Entity\User;

//hacher les mot de passe 
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
class AppFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(private UserPasswordHasherInterface $passwordHasher)
    {
    }
    public function load(ObjectManager $manager): void
    {

        
        
         $user1 = new User();
         $user1->setEmail('user1@gmail.com');
         $hashedPassword = $this->passwordHasher->hashPassword($user1,'user1');
         $user1->setPassword($hashedPassword);

         $user2 = new User();
         $user2->setEmail('user2@gmail.com');
         $hashedPassword = $this->passwordHasher->hashPassword($user2,'user2');
         $user2->setPassword($hashedPassword);

         $admin = new User();
         $admin->setEmail('admin@gmail.com');
         $admin->setRoles(['ROLE_ADMIN']);

        // Encodez le mot de passe de l'utilisateur
        $hashedPassword = $this->passwordHasher->hashPassword($admin,'admin');
        $admin->setPassword($hashedPassword);

            //creation des albums
        for ($i = 1; $i <= 3; $i++) {
            $album = new Album();
            $album->setTitre('album '.$i);
            $album->setDescription('description album_'.$i);
            $album->setSlug('album-'.$i);
            if ($i % 3 == 0) {
                $album->setIsPublic(false); 
                $album->setUser($user2);
            } else {
                $album->setIsPublic(true);
                $album->setUser($user1);
            }

            //creation des images 

            for ($j = 1; $j <= 3; $j++) {
                $image = new Image();
                $image->setTitre('image '.$j);
                $image->setDescription('description image_'.$j);
                $image->setDateCreation(new \DateTime());
                $image->setAlbum($album);
                $manager->persist($image);
            }

            $manager->persist($album);
        }
        


        $manager->persist($admin);
        $manager->persist($user1);
        $manager->persist($user2);

        $manager->flush();
    }
}
